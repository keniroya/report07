/*
  名前 : my_namecards.c
  概要 : 名簿リストの統計解析
  引数 : なし
  返値 : なし
  履歴 : 2015/12/4  ver. 1.0 久保見 作成
*/

#include "my_namecards.h"

/*
  名前 : my_read_tsv_file
  概要 : tsvファイルを一行ずつ読み込み順にListNodeへ追加していく
  引数 : (入出力) ListNode *head
  返値 : なし
  履歴 : 2015/12/4  ver. 1.0 久保見 作成
*/

ListNode *my_read_tsv_file(ListNode *head)
{
  FILE *fp;
  char fname[128] = "meibo.tsv";
  char str[128];
  ListNode *node_p, *node;

  node_p = head;
  fp = fopen(fname, "r");
  if(fp == NULL){
    fprintf(stderr, " ! 「%s」が開けません\n", fname);
    exit(-1);
  }
  while(fgets(str, 128, fp) != NULL){
    node = my_cards_add_new_node(str);
    node_p->next = node;
    node_p = node;
  }
  fclose(fp);
  printf(" * 「%s」を読み込みました\n", fname);
  
  return head;
}

/*
  名前 : my_cards_add_new_node
  概要 : 新しくリストに追加するノードを1つ分領域確保し値を代入する
  引数 : (入力) char *str            読み込んだ一行すべての文字列
  返値 : (出力) char ListNode *node  新しいノードのアドレス
  履歴 : 2015/12/4  ver. 1.0 久保見 作成
*/

ListNode *my_cards_add_new_node(char *str)
{
  ListNode *node;
  node = (ListNode *) malloc(sizeof(ListNode));	// mallocで1つ分確保
  if (node == NULL) {
    fprintf(stderr, " ! 名簿のための領域確保が出来ませんでした\n");
    exit(-1);
  }
  
  strcpy(node->code, my_pop_str_tab_stationary(str));
  strcpy(node->name, my_pop_str_tab_stationary(str));
  strcpy(node->kana, my_pop_str_tab_stationary(str));
  node->option = my_split_str_tab_options(str);
  node->next = NULL;

  return node;
}

/*
  名前 : my_pop_str_tab_stationary
  概要 : tab区切りの文字列から先頭の一つを返値として取り出し、残りを引数から返す
  引数 : (入出力) char str   分割前のtab区切り文字列 -> 分割した残りを出力
  返値 : (出力) char result  分割後の戦闘文字列
  履歴 : 2015/12/4  ver. 1.0 久保見 作成
*/

char *my_pop_str_tab_stationary(char *str)
{
  char *result, *start_p, *str_p;
  int len;

  str_p = str;
  start_p = str_p;
  
  str_p = strchr(start_p, '\t');
  if (str_p == NULL) {
    str_p = strchr(start_p, '\0');
  }
  
  if(strncmp(start_p, "#", 1) == 0)
    start_p++;
  len = str_p - start_p;
  result = (char *) malloc(sizeof(char) * (len + 1));
  memmove(result, start_p, len);
  result[len] = '\0';

  memmove(str, str_p + 1, 128);

  return result;
}

/*
  名前 : my_split_str_tab_options
  概要 : tab区切りの文字列を、ニ次元配列に代入する（オプション部分）
  引数 : (入力) char str        分割前のtab区切り文字列
  返値 : (出力) char **options 分割後のニ次元配列
  注意 : 返す配列は関数内で領域確保して作られる
  履歴 : 2015/12/4  ver. 1.0 久保見 作成
*/

char **my_split_str_tab_options(char *str)
{
  char **options;
  char **wc_info; // 列数カウント用
  char *str_p, *str_t;
  int rsize, count, i;

  // 列数カウント
  wc_info = my_split_str_tab(str, &rsize);
  for (i = 0; i < rsize; i++) { // mallocのfree
    free(wc_info[i]);
  }
  free(wc_info);

  // 値を配列に格納
  options = (char **) malloc(sizeof(char *) * (rsize));   // 領域確保
  // malloc出来なかったときの例外処理
  if(options == NULL) {
    printf(" ! メモリが確保できませんでした\n");
    exit(-1);
  }
  
  for(count=0; count < rsize; count++){
    str_p = str; // 検索にヒットした要素のポインタ
    str_t = str_p; // ひとつ前の要素のポインタ
    
    while ((str_p = strchr(str_p, '\t')) != NULL) {
      options[count] = (char *) malloc(sizeof(char) * 128);   // 領域確保
      // malloc出来なかったときの例外処理
      if(options[count] == NULL) {
	printf(" ! メモリが確保できませんでした\n");
	exit(-1);
      }
      sscanf(str_t,"%[^\t]",options[count]); //tab以外の文字列を読み込む
      str_t = ++str_p;
      count++;                    // 要素数をカウント
    }
    
    // 最後に、文末の要素の処理
    options[count] = (char *) malloc(sizeof(char) * 128);   // 領域確保
    // malloc出来なかったときの例外処理
    if(options[count] == NULL) {
      printf(" ! メモリが確保できませんでした\n");
      exit(-1);
    }
    sscanf(str_t,"%[^\n]",options[count]);
  }

  return options;
}

/*
  名前 : my_split_str_tab
  概要 : 入力から取得した文字列をTABで区切られていたデータ数に分割し二次元配列で返す
  引数 : (入力) char *str_org 対象文字列:変更不可
               int *size_p データ数
  返値 : (出力) char ** 分割した文字列の二次元配列
  履歴 : 2015/10/21 ver. 1.0 久保見 作成 
         2015/10/28 ver. 1.1 久保見 修正
*/
char **my_split_str_tab(char *str_org, int *size_p)
{

  char **newstr;
  char *str_p, *str_t;
  int count;

  // 要素数カウント
  str_p = str_org; // 検索にヒットした要素のポインタ
  count = 1;
  while ((str_p = strchr(str_p, '\t')) != NULL) {
    str_p++;
    count++;
  }
  // ************
  
  newstr = (char **) malloc(sizeof(char*) * count);   // 返り値用領域確保
  // malloc出来なかったときの例外処理
  if(newstr == NULL) {
    printf(" ! メモリが確保できませんでした\n");
    exit(-1);
  }

  str_p = str_org; // 検索にヒットした要素のポインタ
  str_t = str_p; // ひとつ前の要素のポインタ
  count = 1;
  while ((str_p = strchr(str_p, '\t')) != NULL) {
    newstr[count-1] = (char *) malloc(sizeof(char) * 128);   // 領域確保
    // malloc出来なかったときの例外処理
    if(newstr[count-1] == NULL) {
      printf(" ! メモリが確保できませんでした\n");
      exit(-1);
    }
    sscanf(str_t,"%[^\t]",newstr[count-1]); //tab以外の文字列を読み込む

    str_t = ++str_p;
    count++;                    // 要素数をカウント
  }

  // 最後に、文末の要素の処理
  newstr[count-1] = (char *) malloc(sizeof(char) * 128);   // 領域確保
  // malloc出来なかったときの例外処理
  if(newstr[count-1] == NULL) {
    printf(" ! メモリが確保できませんでした\n");
    exit(-1);
  }
  sscanf(str_t,"%[^\n]",newstr[count-1]);

  *size_p = count;
  
  return (newstr);

}

/*
  名前 : my_list_menu
  概要 : メニューの選択を促す
  引数 : (出力) Menu *menu
  返値 : なし
  動作 : 機能[一覧]を選択した場合、menu->mainに1を代入する
  履歴 : 2015/12/4  ver. 1.0 久保見 作成
*/

Menu *my_list_menu(ListNode *head)
{
  Menu *menu;
  int num = 0;
  
  menu = NULL;

  printf(" * メニューを表示します\n");
  printf(" * %d(一覧),", M_PRINT);
  printf(" %d(昇順),", M_ASC);
  printf(" %d(降順),", M_DESC);
  printf(" %d(検索),", M_SEARCH);
  printf(" %d(グラフ),", M_GRAPH);
  printf(" %d(書出),", M_WRITE);
  printf(" %d(再読込),", M_RESET);
  printf(" %d(終了)\n", M_EXIT);

  if (SIG_ERR == signal(SIGINT, my_cards_exit)) {
    exit(-1);
  }

  if(num != M_EXIT){
    printf(" > 選択してください : ");
    scanf("%d", &num);
    menu = my_list_menu_branch(head, num);
  }

  return menu;
}

Menu *my_list_menu_branch(ListNode *head, int num)
{
  Menu *menu;
  menu = (Menu *) malloc(sizeof(Menu) * 1);   // 領域確保
  // malloc出来なかったときの例外処理
  if(menu == NULL) {
    printf(" ! メモリが確保できませんでした\n");
    exit(-1);
  }

  menu->main = num;

  switch(menu->main) {
  case M_PRINT:
    my_cards_print(head);
    break;
  case M_ASC:
    my_list_menu_row(menu);
    my_cards_asc(head, menu);
    break;
  case M_DESC:
    my_list_menu_row(menu);
    my_cards_desc(head, menu);
    break;
  case M_SEARCH:
    my_list_menu_row(menu);
    my_list_menu_option(menu);
    my_cards_search(head, menu);
    break;
  case M_GRAPH:
    my_list_menu_row(menu);
    my_cards_graph(head, menu);
    break;
  case M_WRITE:
    my_list_menu_option(menu);
    my_cards_write(head, menu);
    break;
  case M_RESET:
    my_cards_reset(head, menu);
    break;
  case M_EXIT:
    my_cards_exit(-1);
    break;
  default:
    printf(" ! 入力値が不正です\n");
    break;
  }

  return menu;
}

/*
  名前 : my_list_menu_row
  概要 : メニュー（機能）を実行する列の選択を促す
  引数 : (入出力) Menu *menu メニューの識別構造体
  返値 : なし
  注意 : 列の数え方は左側から順に1,2,3,...とする
  例外 : 1) 機能[一覧]が指定されていた場合、この関数は呼び出されない
         2) 機能[終了]が指定されていた場合、この関数は呼び出されない
         3) 機能[書出]が指定されていた場合、何もせずmy_list_menu_option関数を呼び出す
  動作 : 左側から3番目の列を選択した場合、menu->rowに3を代入する
  履歴 : 2015/12/4  ver. 1.0 久保見 作成
*/

void my_list_menu_row(Menu *menu)
{
  printf(" > 列番号を入力してください : ");
  scanf("%d", &menu->row);
}

/*
  名前 : my_list_menu_option
  概要 : メニュー（機能）のオプションの選択を促す
  引数 : (入出力) Menu *menu メニューの識別構造体
  返値 : なし
  注意 : 引数として指定できる上限はchar[64]まで
  例外 : オプションの選択が不要な場合、この関数は呼び出されない
  動作 : 機能[書出]が指定されていてtsvを選択した場合、menu->optionにtsvが代入される
  履歴 : 2015/12/4  ver. 1.0 久保見 作成
*/

void my_list_menu_option(Menu *menu)
{
  printf(" > オプションを入力してください : ");
  scanf("%s", menu->option);
}

/*
  名前 : my_cards_print
  概要 : 名簿の一覧をモニタ出力する
  引数 : (入力) ListNode *head 名簿リストのヘッダ
         (入力) Menu *menu メニューの識別構造体
  返値 : なし
  履歴 : 2015/12/4  ver. 1.0 久保見 作成
*/

void my_cards_print(ListNode *head)
{
  ListNode *node = NULL;
  int count = 0, i;
  
  node = head->next;
  printf(" * 名簿一覧を表示します\n");
  
  while(node != NULL){
    if(count++ == 0){
      printf(" *      ");
      printf("[%s]\t", node->code);
      printf("[%s]\t", node->name);
      printf("[%s]", node->kana);
      for(i=0;node->option[i] != NULL; i++)
	printf("\t[%s]", node->option[i]);
    }else{
      printf(" * %4d ", count);
      printf("%s\t", node->code);
      printf("%s\t", node->name);
      printf("%s", node->kana);
      for(i=0;node->option[i] != NULL; i++)
	printf("\t%s", node->option[i]);
    }
    printf("\n");
    node = node->next;      // リストの次のアドレスへ
  }
}

/*
  名前 : my_cards_asc
  概要 : 名簿を指定された列で昇順並べ替えをする
  引数 : (入力) ListNode *head 名簿リストのヘッダ
         (入力) Menu *menu メニューの識別構造体
  返値 : なし
  履歴 : 2015/12/4  ver. 1.0 久保見 作成
*/

void my_cards_asc(ListNode *head, Menu *menu)
{
  ListNode *node_p, *node, *node_n;
  char *str1, *str2;
  int count, i;

  printf(" * 昇順に並べ替えをします\n");
  do{
    count=0; i=0;
    node_p = head->next;          // リストの1つめのアドレスを代入
    node = node_p->next;
    node_n = node->next;  
    while (node_n != NULL) {      // 終端(NULL)まで回す
      if(menu->row == 1){      
	str1 = node->code;
	str2 = node_n->code;
      }else if(menu->row == 2){
	str1 = node->name;
	str2 = node_n->name;
      }else if(menu->row == 3){
	str1 = node->kana;
	str2 = node_n->kana;
      }else{
	while(node->option[i] != NULL){
	  i++;
	}
	str1 = node->option[menu->row-4];
	str2 = node_n->option[menu->row-4];
      }
      if(i>(menu->row-4)){
	if(strcmp(str1, str2) > 0){
	  node_p->next = node_n;
	  node->next = node_n->next;
	  node_n->next = node;
	  node_p = node_p->next;      // リストの次のアドレスへ
	  node = node_n->next;
	  node_n = node->next;
	  count++;
	}else{
	  node_p = node_p->next;      // リストの次のアドレスへ
	  node = node->next;
	  node_n = node_n->next;
	}
      }else{
	printf(" ! 正しい列を入力してください\n");
	node_n = NULL; count = 0;
      }
    }
  }while(count > 0);
}

/*
  名前 : my_cards_desc
  概要 : 名簿を指定された列で降順並べ替えをする
  引数 : (入力) ListNode *head 名簿リストのヘッダ
         (入力) Menu *menu メニューの識別構造体
  返値 : なし
  履歴 : 2015/12/4  ver. 1.0 久保見 作成
*/

void my_cards_desc(ListNode *head, Menu *menu)
{
  ListNode *node_p, *node, *node_n;
  char *str1, *str2;
  int count, i;

  printf(" * 降順に並べ替えをします\n");
  do{
    count=0; i=0;
    node_p = head->next;          // リストの1つめのアドレスを代入
    node = node_p->next;
    node_n = node->next;  
    while (node_n != NULL) {      // 終端(NULL)まで回す
      if(menu->row == 1){      
	str1 = node->code;
	str2 = node_n->code;
      }else if(menu->row == 2){
	str1 = node->name;
	str2 = node_n->name;
      }else if(menu->row == 3){
	str1 = node->kana;
	str2 = node_n->kana;
      }else{
	while(node->option[i] != NULL){
	  i++;
	}
	str1 = node->option[menu->row-4];
	str2 = node_n->option[menu->row-4];
      }
      if(i>(menu->row-4)){
	if(strcmp(str1, str2) < 0){
	  node_p->next = node_n;
	  node->next = node_n->next;
	  node_n->next = node;
	  node_p = node_p->next;      // リストの次のアドレスへ
	  node = node_n->next;
	  node_n = node->next;
	  count++;
	}else{
	  node_p = node_p->next;      // リストの次のアドレスへ
	  node = node->next;
	  node_n = node_n->next;
	}
      }else{
	printf(" ! 正しい列を入力してください\n");
	node_n = NULL; count = 0;
      }
    }
  }while(count > 0);
}

/*
  名前 : my_cards_search
  概要 : 名簿を指定された列で検索をかける
  引数 : (入力) ListNode *head 名簿リストのヘッダ
         (入力) Menu *menu メニューの識別構造体
  返値 : なし
  動作 : 検索にヒットしたノードはコピーされ、新しいリストに追加される
  履歴 : 2015/12/5  ver. 1.0 久保見 作成
*/

void my_cards_search(ListNode *head, Menu *menu)
{
  ListNode *node_p, *node;
  char *str, str0[64]="", str1[64]="", str2[64]="";
  char *str_p, *str_q;
  int count, i;
  
  printf(" * 検索にヒットした列のみ抽出します\n");
  count=0; i=0;
  str_p = menu->option;
  str_q = strchr(str_p, '-');

  if((str_q - str_p) == 0){
    strncpy(str2, str_p + 1, (int)strlen(str_p) - 1);
  }else if((str_q - str_p) > 0){
    if((str_q - str_p) == (strlen(str_p) - 1)){
      strncpy(str1, str_p, (int)strlen(str_p) - 1);
    }else{
      strncpy(str1, str_p, (int)(str_q - str_p));
      strncpy(str2, str_q + 1, (int)strlen(str_p) - (str_q - str_p) - 1);
    }
  }else{
    strncpy(str0, str_p, (int)strlen(str_p));
  }

  if(strlen(str0)>0){
    node_p = head->next;          // リストの1つめのアドレスを代入
    node = node_p->next;  
    while (node != NULL) {      // 終端(NULL)まで回す
      if(menu->row == 1){      
	str = node->code;
      }else if(menu->row == 2){
	str = node->name;
      }else if(menu->row == 3){
	str = node->kana;
      }else{
	while(node->option[i] != NULL){
	  i++;
	}
	str = node->option[menu->row-4];
      }
      if(i>(menu->row-4)){
	if(strcmp(str, str0) == 0){
	  node_p->next = node;
	  node_p = node_p->next;
	  count++;
	}
	node = node->next;
      }else{
	printf(" ! 正しい列を入力してください\n");
	node = NULL;
      }
    }
    node_p->next = NULL;
  }else if(strlen(str1)>0 && strlen(str2)>0){
    node_p = head->next;          // リストの1つめのアドレスを代入
    node = node_p->next;  
    while (node != NULL) {      // 終端(NULL)まで回す
      if(menu->row == 1){      
	str = node->code;
      }else if(menu->row == 2){
	str = node->name;
      }else if(menu->row == 3){
	str = node->kana;
      }else{
	while(node->option[i] != NULL){
	  i++;
	}
	str = node->option[menu->row-4];
      }
      if(i>(menu->row-4)){
	if(strncmp(str, str1, strlen(str1)) >= 0){
	  if(strncmp(str, str2, strlen(str2)) <= 0){
	    node_p->next = node;
	    node_p = node_p->next;
	    count++;
	  }
	}
	node = node->next;
      }else{
	printf(" ! 正しい列を入力してください\n");
	node = NULL;
      }
    }
    node_p->next = NULL;
  }else if(strlen(str1)>0 && strlen(str2)==0){
    node_p = head->next;          // リストの1つめのアドレスを代入
    node = node_p->next;  
    while (node != NULL) {      // 終端(NULL)まで回す
      if(menu->row == 1){      
	str = node->code;
      }else if(menu->row == 2){
	str = node->name;
      }else if(menu->row == 3){
	str = node->kana;
      }else{
	while(node->option[i] != NULL){
	  i++;
	}
	str = node->option[menu->row-4];
      }
      if(i>(menu->row-4)){
	if(strncmp(str, str1, strlen(str1)) >= 0){
	  node_p->next = node;
	  node_p = node_p->next;
	  count++;
	}
	node = node->next;
      }else{
	printf(" ! 正しい列を入力してください\n");
	node = NULL;
      }
    }
    node_p->next = NULL;
  }else if(strlen(str1)==0 && strlen(str2)>0){
    node_p = head->next;          // リストの1つめのアドレスを代入
    node = node_p->next;  
    while (node != NULL) {      // 終端(NULL)まで回す
      if(menu->row == 1){      
	str = node->code;
      }else if(menu->row == 2){
	str = node->name;
      }else if(menu->row == 3){
	str = node->kana;
      }else{
	while(node->option[i] != NULL){
	  i++;
	}
	str = node->option[menu->row-4];
      }
      if(i>(menu->row-4)){
	if(strncmp(str, str2, strlen(str2)) <= 0){
	  node_p->next = node;
	  node_p = node_p->next;
	  count++;
	}
	node = node->next;
      }else{
	printf(" ! 正しい列を入力してください\n");
	node = NULL;
      }
    }
    node_p->next = NULL;
  }
}
/*
  名前 : my_cards_graph
  概要 : 名簿を指定された列でモニタにグラフ出力をする
  引数 : (入力) ListNode *head 名簿リストのヘッダ
         (入力) Menu *menu メニューの識別構造体
  返値 : なし
  履歴 : 2015/12/6  ver. 1.0 久保見 作成
*/

void my_cards_graph(ListNode *head, Menu *menu)
{
  ListNode *node_p, *node;
  char *str;
  int count=0, max=0, i=0, j, k=0;

  node = head->next->next;
  while (node != NULL) {      // 終端(NULL)まで回す
    max++;
    node = node->next;
  }
  
  int check[max];
  char graph[max][128];
  for(j=0; j<max; j++)
    check[j]=0;

  printf(" * グラフを表示します\n");
  node_p = head->next;          // リストの1つめのアドレスを代入
  node = node_p->next;
  while (node != NULL) {      // 終端(NULL)まで回す
    if(menu->row == 1){      
      str = node->code;
    }else if(menu->row == 2){
      str = node->name;
    }else if(menu->row == 3){
      str = node->kana;
    }else{
      while(node->option[i] != NULL){
	i++;
      }
      str = node->option[menu->row-4];
    }
    if(i>(menu->row-4)){
      for(j=0;j<k;j++){
	if(strcmp(graph[j],str) == 0){
	  check[j] += 1;
	  count = 1;
	}
      }
      if(count == 0){
	strcpy(graph[k], str);
	check[k]++;
	k++;
      }
      node = node->next;
      count = 0;
    }else{
      printf(" ! 正しい列を入力してください\n");
      node = NULL; count = -1;
    }
  }

  max=0;
  if(count != -1){
    for(i=0; i<=k; i++){
      if(max < check[i])
	max = check[i];
    }
    for(i=0; i<k; i++){
      printf("%-12s:", graph[i]);
      printf("%-3d:", check[i]);
      for(j=0; j<check[i]*30/max; j++){
	printf("+");
      }
      printf("\n");
    }
  }
}

/*
  名前 : my_cards_write
  概要 : 名簿を指定された拡張子でファイル出力をする
  引数 : (入力) ListNode *head 名簿リストのヘッダ
         (入力) Menu *menu メニューの識別構造体
  返値 : なし
  履歴 : 2015/12/5  ver. 1.0 久保見 作成
*/

void my_cards_write(ListNode *head, Menu *menu)
{
  FILE *fp;
  char fname[128] = "meibo_output.";
  char split[4] = "";
  ListNode *node_p;
  int count = 0, i;

  if(strcmp(menu->option, "tsv") == 0){
    strncpy(split, "\t", strlen("\t"));
    strcat(fname, menu->option);
  }else if(strcmp(menu->option, "csv") == 0){
    strncpy(split, ",", strlen(","));
    strcat(fname, menu->option);
  }else{
    fprintf(stderr, " ! 正しいオプションを指定してください\n");
    exit(-1);
  }

  fp = fopen(fname, "w");
  if(fp == NULL){
    fprintf(stderr, " ! 「%s」が開けません\n", fname);
    exit(-1);
  }
  node_p = head->next;          // リストの1つめのアドレスを代入
  printf(" * 「%s」に名簿を書き出します\n", fname);
  while (node_p != NULL) {      // 終端(NULL)まで回す
    if(count == 0){
      fprintf(fp, "#%s%s", node_p->code, split);
      fprintf(fp, "%s%s", node_p->name, split);
      fprintf(fp, "%s", node_p->kana);
      for(i=0;node_p->option[i] != NULL; i++)
	fprintf(fp, "%s%s", split, node_p->option[i]);
    }else{
      fprintf(fp, "%s%s", node_p->code, split);
      fprintf(fp, "%s%s", node_p->name, split);
      fprintf(fp, "%s", node_p->kana); 
      for(i=0;node_p->option[i] != NULL; i++)
	fprintf(fp, "%s%s", split, node_p->option[i]);
    }
    fprintf(fp, "\n");
    node_p = node_p->next;      // リストの次のアドレスへ
    count++;
  }
}

/*
  名前 : my_cards_reset
  概要 : 名簿読み込みの関数を再度呼び出す
  引数 : (入力) ListNode *head 名簿リストのヘッダ
         (入力) Menu *menu メニューの識別構造体
  返値 : なし
  履歴 : 2015/12/4  ver. 1.0 久保見 作成
*/

void my_cards_reset(ListNode *head, Menu *menu)
{
  printf(" * 名簿ファイルを再読込します\n");
  my_read_tsv_file(head);
}

/*
  名前 : my_cards_exit
  概要 : プログラムを終了する旨を表示する
  引数 : (入力) int sig シグナル番号
  返値 : なし
  履歴 : 2015/12/5  ver. 1.0 久保見 作成
*/

void my_cards_exit(int sig)
{
  if(sig >= 0)
    printf("\n");
  printf(" * プログラムを終了します\n");
  exit(-1);
}
