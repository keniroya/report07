/*
  名前 : report7_1.c
  概要 : 名簿リストの統計解析
  引数 : なし
  返値 : なし
  履歴 : 2015/12/4  ver. 1.0 久保見 作成
*/

#include "my_namecards.h"

int main()
{
  ListNode head;
  Menu *menu;

  head.next = NULL;

  my_read_tsv_file(&head);
  do{
    menu = my_list_menu(&head);
  }while(menu->main >= 0);
  
  return (0);
}
