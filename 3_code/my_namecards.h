/*
  名前 : my_namecards.h
  概要 : 名簿リストの統計解析
  引数 : なし
  返値 : なし
  履歴 : 2015/12/4  ver. 1.0 久保見 作成
*/

#ifndef __MY_NAMECARDS_H__
#define __MY_NAMECARDS_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

#define M_PRINT 1  // 機能[一覧]を示す値
#define M_ASC 2    // 機能[昇順]を示す値
#define M_DESC 3   // 機能[降順]を示す値
#define M_SEARCH 4 // 機能[検索]を示す値
#define M_GRAPH 5  // 機能[グラフ]を示す値
#define M_WRITE 6  // 機能[書出]を示す値
#define M_RESET 0  // 機能[再読込]を示す値
#define M_EXIT -1  // 機能[終了]を示す値

/*
  名前 : ListNode
  要素 : char code[128]  社員コード
         char name[128]  名前（漢字）
         char kana[128]  名前（カタカナ）
         char **option   オプション項目（ニ次元配列）
         ListNode *next 次のノードへのポインタ
  概要 : リストのノード用の構造体
*/

typedef struct list_node {
  char code[128];         // 社員コード
  char name[128];         // 名前（漢字）
  char kana[128];         // 名前（カタカナ）
  char **option;          // オプション項目（ニ次元配列）
  struct list_node *next; // 次のノードへのポインタ
} ListNode;

/*
  名前 : Menu
  要素 : int main
         int row
         char option[64]
  概要 : 指定されたメニューを格納する構造体
*/

typedef struct {
  int main;
  int row;
  char option[64];
} Menu;

ListNode *my_read_tsv_file(ListNode *); //行数分繰り返し
ListNode *my_cards_add_new_node(char *); //malloc
char *my_pop_str_tab_stationary(char *); //一文字取り出す
char **my_split_str_tab_options(char *); //残りを配列に

char **my_split_str_tab(char *, int *); //report04からコピー

Menu *my_list_menu(ListNode *);
Menu *my_list_menu_branch(ListNode *, int);
void my_list_menu_row(Menu *);
void my_list_menu_option(Menu *);
void my_cards_print(ListNode *);
void my_cards_asc(ListNode *, Menu *);
void my_cards_desc(ListNode *, Menu *);
void my_cards_search(ListNode *, Menu *);
void my_cards_graph(ListNode *, Menu *);
void my_cards_write(ListNode *, Menu *);
void my_cards_reset(ListNode *, Menu *);

void my_cards_exit(int);

#endif
