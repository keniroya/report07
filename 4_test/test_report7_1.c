/*
  名前 : test_report7_1.c
  概要 : my_namecardsライブラリのテスト
  引数 : なし
  返値 : なし
  履歴 : 2015/12/9 ver. 1.0 久保見 作成
*/

#include "assert.h"    // テスト用マクロのライブラリ
#include "my_namecards.h"

int main()
{
  ListNode head, *node;
  Menu menu, *menu_p;
  char test_str[128], **tmp;
  int num;

  // tsvファイルを読み込めているか
  head.next = NULL;
  my_read_tsv_file(&head);
  assert(head.next != NULL);

  // malloc出来ているか
  node = NULL;
  strcpy(test_str, "B11T2061M	山本	ヤマモト	23	男	team16");
  node = my_cards_add_new_node(test_str);
  assert(node != NULL);

  // 指定列が全て読み込めているか
  strcpy(test_str, "B11T2061M	山本	ヤマモト	23	男	team16");
  assert(strcmp(my_pop_str_tab_stationary(test_str), "B11T2061M") == 0);
  assert(strcmp(my_pop_str_tab_stationary(test_str), "山本") == 0);
  assert(strcmp(my_pop_str_tab_stationary(test_str), "ヤマモト") == 0);

  // 自由列が全て読み込めているか
  tmp = my_split_str_tab_options(test_str);
  assert(strcmp(tmp[0], "23") == 0);
  assert(strcmp(tmp[1], "男") == 0);
  assert(strcmp(tmp[2], "team16") == 0);

  // tabで区切るといくつの単語に分かれるか、判別可能か
  strcpy(test_str, "B11T2061M	山本	ヤマモト	23	男	team16");
  tmp = my_split_str_tab(test_str, &num);
  assert(num == 6);

  // メニューの選択がうまくいくか
  //menu.main = 0;
  //my_list_menu(&head, &menu);
  //void my_list_menu_row(Menu *);
  //void my_list_menu_option(Menu *);

  // 機能[一覧]がうまくいくか
  printf(" > メニュー : 1(一覧)\n");
  menu_p = my_list_menu_branch(&head, M_PRINT, 0, "");

  // 機能[昇順]がうまくいくか
  printf(" > メニュー : 2(昇順)\n > 列番号 : 1\n");
  menu_p = my_list_menu_branch(&head, M_ASC, 1, "");
  menu_p = my_list_menu_branch(&head, M_PRINT, 0, "");
  printf(" > メニュー : 2(昇順)\n > 列番号 : 2\n");
  menu_p = my_list_menu_branch(&head, M_ASC, 2, "");
  menu_p = my_list_menu_branch(&head, M_PRINT, 0, "");
  printf(" > メニュー : 2(昇順)\n > 列番号 : 3\n");
  menu_p = my_list_menu_branch(&head, M_ASC, 3, "");
  menu_p = my_list_menu_branch(&head, M_PRINT, 0, "");
  printf(" > メニュー : 2(昇順)\n > 列番号 : 4\n");
  menu_p = my_list_menu_branch(&head, M_ASC, 4, "");
  menu_p = my_list_menu_branch(&head, M_PRINT, 0, "");

  // 機能[降順]がうまくいくか
  printf(" > メニュー : 3(降順)\n > 列番号 : 1\n");
  menu_p = my_list_menu_branch(&head, M_DESC, 1, "");
  menu_p = my_list_menu_branch(&head, M_PRINT, 0, "");
  printf(" > メニュー : 3(降順)\n > 列番号 : 2\n");
  menu_p = my_list_menu_branch(&head, M_DESC, 2, "");
  menu_p = my_list_menu_branch(&head, M_PRINT, 0, "");
  printf(" > メニュー : 3(降順)\n > 列番号 : 3\n");
  menu_p = my_list_menu_branch(&head, M_DESC, 3, "");
  menu_p = my_list_menu_branch(&head, M_PRINT, 0, "");
  printf(" > メニュー : 3(降順)\n > 列番号 : 4\n");
  menu_p = my_list_menu_branch(&head, M_DESC, 4, "");
  menu_p = my_list_menu_branch(&head, M_PRINT, 0, "");

  // 機能[検索]がうまくいくか
  //menu_p = my_list_menu_branch(&head, M_DESC, 1, "");
  //menu_p = my_list_menu_branch(&head, M_DESC, 2, "");
  //menu_p = my_list_menu_branch(&head, M_DESC, 3, "");
  //menu_p = my_list_menu_branch(&head, M_DESC, 4, "");
  
  /*
    void my_cards_desc(ListNode *, Menu *);
    void my_cards_search(ListNode *, Menu *);
    void my_cards_graph(ListNode *, Menu *);
    void my_cards_write(ListNode *, Menu *);
    void my_cards_reset(ListNode *, Menu *);
    
    void my_cards_exit(int);
  */

  return(0);
}
