このフォルダ内に
spec.txt あるいは
spec.pdf あるいは
spec_require.pdf
等のファイル名でレポートとなる仕様書を用意する事。
画像付がより良いので、できればPDF形式が望ましい。

また、コンテキストレベルのデータフロー図として
dfd.pdf (あるいは dfd.png あるいは dfd.jpg)
を用意する事。

=====
* 要求仕様書 requirement specification
  * 基本的な機能等
  * 特徴的な機能等
  * 動作条件等
  * 詳細なデータ構造／関数一覧等は省く(別途記述する)

  * 納入先との初期打合せを想定。
  * 電気屋店頭等で得られるパンフレットを想定。
  * A4カラー数枚、図付
